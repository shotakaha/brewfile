# brewfile

My Brewfile

## Make Brewfile

```bash
$ brew bundle dump
$ less Brewfile
```

## Install from Brewfile

```bash
$ brew bundle
```
